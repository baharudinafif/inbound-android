package com.inbound;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	TextView messageText;
	Button uploadButton;
	Button captureButton;
	ImageView capturedImage;
	AppFolderManagement appFolder;
	UserLocation userPos;
	UploadData uploadData;

	int serverResponseCode = 0;
	ProgressDialog dialog = null;

	String upLoadServerUri = null;

	/********** File Path *************/
	String uploadFileName = "IMG.jpg";
	String uploadFilePath = Environment.getExternalStorageDirectory()
			.getAbsolutePath() + File.separator + "InBound/temp/";
	String uploadFileFull = uploadFilePath + uploadFileName;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_upload_to_server);

		uploadButton = (Button) findViewById(R.id.uploadButton);
		messageText = (TextView) findViewById(R.id.messageText);
		captureButton = (Button) findViewById(R.id.captureButton);
		capturedImage = (ImageView) findViewById(R.id.captured);

		userPos = new UserLocation(MainActivity.this);

		System.out.println(uploadFileName);
		System.out.println(uploadFilePath);
		System.out.println(uploadFileFull);

		messageText.setText("Uploading file path :- " + uploadFileFull);

		// Create APPFolder
		appFolder = new AppFolderManagement("InBound");
		appFolder.createFolder("InBound/temp");

		/************* Php script path ****************/
		upLoadServerUri = "http://192.168.36.1/inbound-web/public/addnewreport";

		// create object UploadData
		uploadData = new UploadData();
		uploadData.setSourceFileUri(uploadFileFull);
		uploadData.setCategory_id(1);
		
		// GPS Operation;
		if (userPos.canGetLocation()) {
			uploadData.setLongitude(userPos.getLongitude());
			uploadData.setLatitude(userPos.getLatitude());
			Toast.makeText(
					getApplicationContext(),
					"Posisi Anda - \n" + "Lat: " + uploadData.getLatitude() + "\n" + "Long: "
							+ uploadData.getLongitude(), Toast.LENGTH_LONG).show();
		} else {
			userPos.showSettingsAlert();
		}

		captureButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				openCamera();
			}
		});

		uploadButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog = ProgressDialog.show(MainActivity.this, "",
						"Uploading file...", true);

				new Thread(new Runnable() {
					public void run() {
						runOnUiThread(new Runnable() {
							public void run() {
								messageText
										.setText("Sabar ya, baru ngupload........");
							}
						});

						uploadFile(uploadData);

					}
				}).start();
			}
		});
	}

	public void openCamera() {
		System.out.print("openCamera : FilePath = " + uploadFileFull);
		File imageFile = new File(uploadFileFull);
		Uri imageFileUri = Uri.fromFile(imageFile); // convert path to Uri

		Intent it = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		it.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, imageFileUri);
		startActivityForResult(it, 23);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if ((requestCode == 23) && (resultCode == RESULT_OK)) {
			System.out
					.println("onActivityResult : filePath= " + uploadFileFull);
			BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
			bmpFactoryOptions.inJustDecodeBounds = false;

			// Saving Caputered Image to SDCard (High Res)
			Bitmap bmp = BitmapFactory.decodeFile(uploadFileFull,
					bmpFactoryOptions);

			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			bmp.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
			capturedImage.setImageBitmap(bmp);

			// you can create a new file name "test.jpg" in sdcard folder.
			File f = new File(uploadFileFull);
			try {
				f.createNewFile();
				// write the bytes in file
				FileOutputStream fo = new FileOutputStream(f);
				fo.write(bytes.toByteArray());

				// remember close de FileOutput
				fo.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void addParam(String boundary,DataOutputStream dos, String ParamName, String ParamValue) throws IOException{			
		String twoHyphens = "--";
		
		dos.write( (twoHyphens + boundary + "\r\n").getBytes());
		dos.write( ("Content-Disposition: form-data; name=\"" + ParamName+"\"\r\n").getBytes());
		dos.write( ("\r\n"+ParamValue+"\r\n").getBytes());

	}

	public int uploadFile(UploadData upData) {

		String fileName = upData.getSourceFileUri();
		double longitude = upData.getLongitude();
		double latitude = upData.getLatitude();
		double facebookId = upData.getFacebook_id();
		String report = upData.getReport();
		int categoryId= upData.category_id;
		
			
		System.out.println("DATA YANG AKAN DIUPLOAD");
		System.out.println(fileName);
		System.out.println(longitude);
		System.out.println(latitude);
		System.out.println(facebookId);		
		System.out.println(report);
		System.out.println(categoryId);
		

		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "SwA"+Long.toString(System.currentTimeMillis())+"SwA";
		System.out.println(boundary);
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		File sourceFile = new File(fileName);

		if (!sourceFile.isFile()) {

			dialog.dismiss();

			Log.e("uploadFile", "Source File not exist :" + uploadFileFull);

			runOnUiThread(new Runnable() {
				public void run() {
					messageText
							.setText("Nggak ada file ini :" + uploadFileFull);
				}
			});

			return 0;

		} else {
			try {

				// open a URL connection to the Servlet
				FileInputStream fileInputStream = new FileInputStream(
						sourceFile);
				URL url = new URL(upLoadServerUri);

				// Open a HTTP connection to the URL
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true); // Allow Inputs
				conn.setDoOutput(true); // Allow Outputs				
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Connection", "Keep-Alive");				
				conn.setRequestProperty("Content-Type",	"multipart/form-data;boundary=" + boundary);												
				dos = new DataOutputStream(conn.getOutputStream());
				
				//---------- add param								
				this.addParam(boundary,dos, "FacebookID", facebookId+"");
				this.addParam(boundary,dos, "category_id", categoryId + "");
				this.addParam(boundary,dos, "description", report);				
				this.addParam(boundary,dos, "longitude", longitude+"");
				this.addParam(boundary,dos, "latitude", latitude+"");								
				
				// -------- MULAI ADD FILE
				
				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\"; filename=\""
						+ fileName +"\""+  lineEnd);
				dos.writeBytes("Content-Type: application/octet-stream" + lineEnd);
				dos.writeBytes("Content-Transfer-Encoding: binary" + lineEnd);
				dos.writeBytes(lineEnd);				

				// create a buffer of maximum size
				bytesAvailable = fileInputStream.available();

				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];

				// read file and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				while (bytesRead > 0) {

					dos.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);
				}				

				dos.writeBytes(lineEnd);
				//---------- KELAR ADD FILE
				
				// send multipart form data necesssary after file data...				
				dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

				// Responses from the server (code and message)
				serverResponseCode = conn.getResponseCode();
				String serverResponseMessage = conn.getResponseMessage();

				Log.i("uploadFile", "HTTP Response is : "
						+ serverResponseMessage + ": " + serverResponseCode);

				if (serverResponseCode == 200) {

					runOnUiThread(new Runnable() {
						public void run() {

							String msg = "Kelar ngupload.\n\n Cek kesini deh : \n\n"
									+ upLoadServerUri;

							messageText.setText(msg);
							Toast.makeText(MainActivity.this,
									"File Upload Complete.", Toast.LENGTH_SHORT)
									.show();
						}
					});
				}

				// close the streams //
				fileInputStream.close();
				dos.flush();
				dos.close();

			} catch (MalformedURLException ex) {

				dialog.dismiss();
				ex.printStackTrace();

				runOnUiThread(new Runnable() {
					public void run() {
						messageText
								.setText("URL tujuan upload nya salah (see uploadServerUri).");
						Toast.makeText(MainActivity.this,
								"MalformedURLException", Toast.LENGTH_SHORT)
								.show();
					}
				});

				Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
			} catch (Exception e) {

				dialog.dismiss();
				e.printStackTrace();

				runOnUiThread(new Runnable() {
					public void run() {
						messageText.setText("Exception : bukak logcat ");
						Toast.makeText(MainActivity.this,
								"Got Exception : see logcat ",
								Toast.LENGTH_SHORT).show();
					}
				});
				Log.e("Upload file to server Exception",
						"Exception : " + e.getMessage(), e);
			}
			dialog.dismiss();
			return serverResponseCode;

		} // End else block
	}
}
