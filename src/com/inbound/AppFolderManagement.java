package com.inbound;

import java.io.File;

import android.os.Environment;

public class AppFolderManagement {
	String folderPath;

	public AppFolderManagement() {
		// TODO Auto-generated constructor stub
		folderPath = "/folder";
	}

	public AppFolderManagement(String folderPath) {
		System.out.println("SDCARD LOCATION : "
				+ Environment.getExternalStorageDirectory().getAbsoluteFile()
				+ File.separator);
		createFolder(folderPath);
	}

	public boolean createFolder(String folderPath) {

		File folder = new File(Environment.getExternalStorageDirectory()
				.getAbsoluteFile() + File.separator + folderPath);
		boolean success = true;
		if (!folder.exists()) {
			success = folder.mkdir();
			System.out.println("Berhasil membuat folder : " + folderPath);
			this.folderPath = folderPath;
			return true;
		}
		return false;
	}
}
